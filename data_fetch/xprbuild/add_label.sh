#!/bin/bash
echo "component_name: ${component_name}"

env $(cat .env | xargs) cat <<EOF | kubectl create -f -
apiVersion: v1
kind: Pod
metadata:
  labels:
    component_name: ${COMPONENT_NAME}

EOF
