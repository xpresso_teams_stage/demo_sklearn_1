"""
    Utility module for run metrics
"""

__all__ = ["MetricTypes"]
__author__ = ["Shlok Chaudhari"]

import enum


class MetricTypes(enum.Enum):
    """
        Supported metric types
    """
    KPIs = "kpis"
    TIME_SERIES = "time_series"
    EXPLAIN_MODEL = "explain_model"

    @staticmethod
    def get_metric_types() -> list:
        return list(map(lambda mt: mt.value, MetricTypes))
