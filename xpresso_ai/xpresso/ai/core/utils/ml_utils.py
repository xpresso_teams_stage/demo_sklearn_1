import re
from sklearn.metrics import mean_absolute_error, mean_squared_error, \
    mean_squared_log_error, median_absolute_error, explained_variance_score, \
    max_error, r2_score, mean_poisson_deviance, mean_gamma_deviance, \
    mean_tweedie_deviance, accuracy_score, balanced_accuracy_score, \
    recall_score, precision_score, f1_score, zero_one_loss, hamming_loss, \
    jaccard_score, matthews_corrcoef, roc_auc_score, average_precision_score, \
    brier_score_loss, coverage_error, label_ranking_average_precision_score, \
    label_ranking_loss, ndcg_score, adjusted_mutual_info_score, \
    adjusted_rand_score, completeness_score, fowlkes_mallows_score, \
    homogeneity_score, mutual_info_score, normalized_mutual_info_score, \
    v_measure_score, calinski_harabasz_score, davies_bouldin_score, \
    silhouette_score

SKLEARN_REGRESSOR_METRICS = [
    mean_absolute_error, mean_squared_error, mean_squared_log_error,
    median_absolute_error, explained_variance_score, max_error, r2_score,
    mean_poisson_deviance, mean_gamma_deviance, mean_tweedie_deviance]
SKLEARN_BINARY_CLASSIFIER_METRICS = [
    accuracy_score, balanced_accuracy_score, recall_score, precision_score,
    f1_score, zero_one_loss, hamming_loss, jaccard_score, matthews_corrcoef]
SKLEARN_CLASSIFIER_PROBABILITY_METRICS = [
    roc_auc_score, average_precision_score, brier_score_loss]
SKLEARN_MULTICLASS_CLASSIFIER_METRICS = [
    accuracy_score, balanced_accuracy_score, zero_one_loss, hamming_loss,
    matthews_corrcoef]
SKLEARN_MULTICLASS_AVERAGING_METRICS = [
    f1_score, recall_score, precision_score, jaccard_score]
SKLEARN_MULTILABEL_CLASSIFIER = [
    accuracy_score, zero_one_loss, hamming_loss]
SKLEARN_MULTILABEL_AVERAGING_METRICS = [
    f1_score, recall_score, precision_score, jaccard_score,
    average_precision_score]
SKLEARN_MULTILABEL_RANKING_METRICS = [
    coverage_error, label_ranking_average_precision_score,
    label_ranking_loss, ndcg_score]
SKLEARN_UNSUPERVISED_CLUSTERING_METRICS = [
    adjusted_mutual_info_score, adjusted_rand_score, completeness_score,
    fowlkes_mallows_score, homogeneity_score, mutual_info_score,
    normalized_mutual_info_score, v_measure_score]
SKLEARN_CLUSTERING_SPECIFIC_METRICS = [
    calinski_harabasz_score, davies_bouldin_score, silhouette_score]


def false_positive_rate(fp, tn):
    """
    Args:
        fp: False Positives (count)
        tn: True Negatives (count)

    Returns: false_positive_rate
    """
    return round(fp / (fp + tn), 2)


def false_negative_rate(fn, tp):
    """
    Args:
        fn: False Negatives (count)
        tp: True Positives (count)

    Returns: false_negative_rate
    """
    return round(fn / (fn + tp), 2)


def true_negative_rate(tn, fp):
    """
    Args:
        tn: True Negatives (count)
        fp: False Positives (count)

    Returns: true_negative_rate
    """
    return round(tn / (tn + fp), 2)


def format_metric_key(metric):
    """
    Reformat metrics name to make sure metric from different libraries to
    be consistent
    Args:
        metric: string, metrics name
    Returns: reformatted metrics name
    """
    
    if "tensorflow.python.keras" in metric.__module__:
        metric_name = metric.__class__.__name__
        metrics_name_split = [w for w in
                              re.split("([A-Z][^A-Z]*)", metric_name) if w]
        reformated_name = " ".join(metrics_name_split)
    else:
        # "sklearn" in metric.__module__ or custom metrics
        metric_name = metric.__name__
        removed_punct = metric_name.replace("_", " ")
        reformated_name = " ".join(
            [w.capitalize() for w in removed_punct.split()])
    return reformated_name
