import logging
import os
from itertools import combinations

import numpy as np
import pandas as pd
import seaborn as sns
import shap
from matplotlib import pyplot as plt, rcParams

from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

logger = XprLogger(name="ShapUtils",
                   level=logging.INFO)
DEFAULT_SHAP_PLOTS = "./shap_images"


def shap_waterfall_plots(expected_value, feature_names,
                         multioutput_n,
                         shap_values, x_explain, random_obs,
                         output_path=DEFAULT_SHAP_PLOTS):
    """SHAP waterfall plots"""
    # make sure expected value is a float value instead of a array
    # containing the float value
    try:
        expected_value = expected_value[0]
    except IndexError:
        pass
    
    # plot the waterfall plots for the random selected observations
    for ob in random_obs:
        try:
            shap.waterfall_plot(expected_value, shap_values[ob],
                                x_explain[ob], feature_names=feature_names,
                                show=False)
            plt.title(
                f"SHAP Waterfall Plot of observation {ob} {multioutput_n}")
            xpresso_save_plot(
                f"shap_waterfall_plot_sample#{ob}{multioutput_n}",
                output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP Waterfall plot")


def shap_dependence_plot(feature_names, multioutput_n, shap_values,
                         x_explain, shap_n_top_features,
                         output_path=DEFAULT_SHAP_PLOTS):
    """dependence_plot for top n most important features"""
    rank_inds = np.argsort(-np.sum(np.abs(shap_values), 0))
    top_inds = rank_inds[:shap_n_top_features]
    for i in top_inds:
        try:
            shap.dependence_plot(feature_names[i], shap_values, x_explain,
                                 feature_names=feature_names, show=False)
            plt.title(
                f"Dependence Plot : {feature_names[i]} {multioutput_n}")
            xpresso_save_plot(
                f"shap_dependence_plot_{feature_names[i]}{multioutput_n}",
                output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP Dependence plot")


def shap_dependence_plot_interaction(feature_names, multioutput_n,
                                     shap_interaction_values, x_explain,
                                     shap_n_top_interaction_features,
                                     output_path=DEFAULT_SHAP_PLOTS):
    """SHAP dependence plot with interaction values"""
    # sort features based on interaction values(absolute)
    tmp_abs = np.abs(shap_interaction_values).sum(0)
    for i in range(tmp_abs.shape[0]):
        tmp_abs[i, i] = 0
    sorted_inds = np.argsort(-tmp_abs.sum(0))
    sorted_features_inter = [feature_names[ind] for ind in sorted_inds]
    
    # Plot n top features interaction values dependence plot, default is 5
    top_features_inter = sorted_features_inter[
                         0:shap_n_top_interaction_features]
    top_feature_combs = []
    for combi in combinations(top_features_inter, 2):
        top_feature_combs.append(combi)
    for i in range(len(top_feature_combs)):
        try:
            fea_0 = top_feature_combs[i][0]
            fea_1 = top_feature_combs[i][1]
            # make plots
            shap.dependence_plot((fea_0, fea_1), shap_interaction_values,
                                 x_explain, feature_names=feature_names,
                                 show=False)
            plt.title(
                f"Dependence Plot Interaction Values: {fea_0} vs {fea_1}")
            plt.show()
            xpresso_save_plot(
                f"shap_dependence_plot_{fea_0}_vs_{fea_1}{multioutput_n}",
                output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP Dependence plot interaction")


def shap_force_plot_collective(expected_value, feature_names, link,
                               multioutput_n, shap_values, x_explain,
                               output_path=DEFAULT_SHAP_PLOTS):
    """SHAP force plot collective"""
    try:
        rcParams['axes.titlepad'] = 24
        force_plot = shap.force_plot(expected_value, shap_values, x_explain,
                                     feature_names=feature_names, link=link)
        shap.save_html(os.path.join(output_path, f"shap_force_plot(collective)"
                                                 f"{multioutput_n}.html"),
                       force_plot)
    except Exception as e:
        logger.info("Unable to plot SHAP force plot")


def shap_inter_values_heatmap(feature_names, multioutput_n,
                              shap_interaction_values,
                              shap_n_top_features,
                              output_path=DEFAULT_SHAP_PLOTS):
    """Heatmap by SHAP Interaction Values"""
    try:
        # sort features based on interaction values(absolute)
        tmp_abs = np.abs(shap_interaction_values).sum(0)
        for i in range(tmp_abs.shape[0]):
            tmp_abs[i, i] = 0
        sorted_inds = np.argsort(-tmp_abs.sum(0))
        # sum interaction values across all samples for each feature
        tmp = shap_interaction_values.sum(0)
        # re-arrange heatmap feature based on interaction values (
        # absoluate value) use top 20 features
        tmp_inds = sorted_inds[0: max(20, shap_n_top_features)]
        tmp_ranked = tmp[tmp_inds, :][:, tmp_inds]
        tem_feature_inter = [feature_names[ind] for ind in tmp_inds]
        
        # plot heatmap
        plt.figure(figsize=(12, 12))
        heat_map = sns.heatmap(tmp_ranked, annot=True, cmap="YlGnBu")
        plt.ylabel('Features')
        heat_map.set_yticklabels(tem_feature_inter, rotation=50.4,
                                 horizontalalignment="right")
        heat_map.set_xticklabels(tem_feature_inter, rotation=50.4,
                                 horizontalalignment="left")
        plt.title(f"Heatmap by SHAP Interaction Values {multioutput_n}")
        plt.gca().xaxis.tick_top()
        plt.show()
        xpresso_save_plot(
            f"shap_heatmap_by_interaction_values{multioutput_n}",
            output_path=output_path)
    except Exception:
        logger.info("Unable to plot SHAP inter values heatmap")


def shap_summary_plot_bar(feature_names, multioutput_n, shap_values,
                          x_explain, output_path=DEFAULT_SHAP_PLOTS):
    """Summary Plot with SHAP mean absolute Values (bar plot)"""
    try:
        shap.summary_plot(shap_values, x_explain,
                          feature_names=feature_names, plot_type="bar",
                          show=False)
        plt.title(f"Feature Importance by SHAP Values(mean absolute value) "
                  f"{multioutput_n}")
        plt.show()
        xpresso_save_plot(f"shap_values_summary_bar_plot{multioutput_n}",
                          output_path=output_path)
    except Exception:
        logger.info("Unable to plot SHAP summary bar plot")


def shap_summary_plot_interaction(feature_names, multioutput_n,
                                  shap_interaction_values, x_explain,
                                  output_path=DEFAULT_SHAP_PLOTS):
    """SHAP summary plot with interaction values"""
    try:
        shap.summary_plot(shap_interaction_values, x_explain,
                          plot_type="compact_dot",
                          feature_names=feature_names, show=False)
        plt.title(f"SHAP Interaction Value Summary Plot {multioutput_n}")
        plt.show()
        xpresso_save_plot(
            f"shap_interaction_values_summary_plot{multioutput_n}",
            output_path=output_path)
    except Exception:
        logger.info("Unable to plot SHAP summary plot interaction")


def shap_decision_plot_interaction(shap_values, expected_value,
                                   feature_names, link, multioutput_n,
                                   shap_interaction_values, x_explain,
                                   output_path=DEFAULT_SHAP_PLOTS):
    """SHAP interaction values decision plot"""
    # use up to 200 samples for better visualization
    sm_x_explain = min(200, len(shap_values))
    
    if not shap_interaction_values:
        return
    try:
        shap.decision_plot(expected_value,
                           shap_interaction_values[0:sm_x_explain],
                           x_explain[0:sm_x_explain],
                           feature_names=feature_names, link=link,
                           show=False)
        plt.title(f"SHAP Interaction Values Decision Plot (collective) "
                  f"{multioutput_n}")
        xpresso_save_plot(
            f"shap_interaction_values_decision_plot_collective"
            f"{multioutput_n}", output_path=output_path)
    except Exception:
        logger.info("Unable to plot SHAP shap_decision_plot_interaction")


def shap_decision_plot_single_instance(expected_value, feature_names,
                                       link, multioutput_n,
                                       shap_values, x_explain, random_obs,
                                       output_path=DEFAULT_SHAP_PLOTS):
    """SHAP decision plot single instance"""
    # Individual Decision Plot
    for ob in random_obs:
        try:
            # plot the SHAP values for the randomly selected observations
            shap.decision_plot(expected_value,
                               shap_values[ob],
                               x_explain[ob],
                               feature_names=feature_names,
                               link=link,
                               show=False)
            plt.title(
                f"SHAP Decision Plot of observation {ob} {multioutput_n}")
            xpresso_save_plot(
                f"shap_decision_plot_sample#{ob}{multioutput_n}",
                output_path=output_path)
        except Exception:
            logger.info(
                "Unable to plot SHAP shap_decision_plot_single_instance")


def shap_decision_plot_collective(expected_value, feature_names, link,
                                  multioutput_n, shap_values, x_explain,
                                  output_path=DEFAULT_SHAP_PLOTS):
    """SHAP decision plot collective"""
    # For better visualization, limit to 200 samples, otherwise SHAP
    # gives a warning
    sm_x_explain = min(200, len(shap_values))
    try:
        rcParams['axes.titlepad'] = 24
        shap.decision_plot(expected_value, shap_values[0:sm_x_explain],
                           x_explain[0:sm_x_explain],
                           feature_names=feature_names, link=link,
                           return_objects=True, show=False)
        plt.title(f"SHAP Decision Plot (collective) {multioutput_n}")
        xpresso_save_plot(
            f"shap_decision_plot_collective{multioutput_n}",
            output_path=output_path)
    except Exception:
        logger.info("Unable to plot SHAP shap_decision_plot_collective")


def shap_force_plot_single_instance(expected_value, feature_names,
                                    link, multioutput_n, shap_values,
                                    x_explain,
                                    shap_n_individual_instance, random_obs,
                                    output_path=DEFAULT_SHAP_PLOTS):
    """SHAP force plot single instance"""
    # Individual force plot
    for ob in random_obs:
        try:
            # plot the SHAP values for the random sampled observations
            rcParams['axes.titlepad'] = 24
            ind_force_plot = shap.force_plot(expected_value,
                                             shap_values[ob],
                                             x_explain[ob],
                                             feature_names=feature_names,
                                             link=link)
            shap.save_html(os.path.join(
                output_path,
                f"shap_force_plot_sample#{ob}{multioutput_n}.html"),
                ind_force_plot)
        except Exception:
            logger.info(
                "Unable to plot SHAP shap_force_plot_single_instance")


def shap_summary_plot_dot(feature_names, multioutput_n, shap_values,
                          x_explain, output_path=DEFAULT_SHAP_PLOTS):
    """Summary Plot with SHAP Values (dot plot)"""
    try:
        shap.summary_plot(shap_values,
                          x_explain,
                          feature_names=feature_names,
                          show=False)
        plt.title(f"Feature Importance by SHAP Values {multioutput_n}")
        plt.ylabel("Features")
        xpresso_save_plot(f"shap_values_summary_plot{multioutput_n}",
                          output_path=output_path)
    except Exception:
        logger.info("Unable to plot SHAP shap_summary_plot_dot")


def save_shap_values_df(feature_names, multioutput_n, shap_values,
                        output_path=DEFAULT_SHAP_PLOTS):
    """Save features and shap values as dataframe in csv file"""
    # Rank features based on SHAP Values (sum of the absolute value of
    # shap values)
    shap_values_abs = (np.abs(shap_values)).sum(axis=0)
    sorted_features = [f for _, f in
                       sorted(zip(shap_values_abs, feature_names),
                              reverse=True)]
    sorted_shap_values = [round(v, 4) for v, _ in
                          sorted(zip(shap_values_abs, feature_names),
                                 reverse=True)]
    # save SHAP values to a dataframe
    try:
        shap_values_df = pd.DataFrame({"features": sorted_features,
                                       "SHAP_values": sorted_shap_values})
        csv_file_name = os.path.join(output_path,
                                     f"shap_values{multioutput_n}.csv")
        shap_values_df.to_csv(csv_file_name)
    except Exception:
        logger.info("Unable to plot SHAP save_shap_values_df")
